# Fork for CAAM519 Homework

## Problem I.5
Modify this Python script to report the linear solver time instead of the nonlinear solve time (“SNESSolve”), and plot it for the GMRES/ILU and GM- RES/GAMG solvers on the same graph. For extra credit, look at the perfor- mance as the number of processes increases.

## Problem I.6 
Modify the script from Problem 5 to report the assembly time, of both the residual and Jacobian, instead of the nonlinear solve time (“SNES-Solve”), and plot it for the GMRES/ILU and GMRES/GAMG solvers on the same graph.