#! /usr/bin/env python
import os

def computePerf(Nx, modname, options, stage, solver_method, index, event):
	os.system('./ex5 ' + ' '.join(options))
	perfmod = __import__(modname)
	size = Nx**2
	time = perfmod.Stages[stage][solver_method][index][event]
	return size, time

# Problem 1.5
# Non linear solver
sizes = []
times = []
residual_times = []
jacobian_times = []
stage = 'Main Stage'
index = 0
event = 'time'
for k in range(5):
	Nx = 10 * 2 ** k
	modname = 'perf%d' % k
	options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname]
	os.system('./ex5 ' + ' '.join(options))
	perfmod = __import__(modname)
	size = Nx**2
	time = perfmod.Stages[stage]['SNESSolve'][index]['time']
	residual_time = perfmod.Stages[stage]['SNESFunctionEval'][index]['time']
	jacobian_time = perfmod.Stages[stage]['SNESJacobianEval'][index]['time']
	sizes.append(size)
	times.append(time)
	residual_times.append(residual_time)
	jacobian_times.append(jacobian_time)
print zip(sizes, times, residual_times, jacobian_times)

# linear solver GMRES/ILU
times_ilu = []
residual_times_ilu = []
jacobian_times_ilu = []
stage = 'Main Stage'
solver_method = 'KSPSolve'
index = 0
event = 'time'
for k in range(5):
	Nx = 10 * 2 ** k
	modname = 'perf_ilu%d' % k
	options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname, 
		'-ksp_type gmres -pc_type ilu']
	os.system('./ex5 ' + ' '.join(options))
	perfmod = __import__(modname)
	size = Nx**2
	time = perfmod.Stages[stage]['KSPSolve'][index]['time']
	residual_time = perfmod.Stages[stage]['SNESFunctionEval'][index]['time']
	jacobian_time = perfmod.Stages[stage]['SNESJacobianEval'][index]['time']
	times_ilu.append(time)
	residual_times_ilu.append(residual_time)
	jacobian_times_ilu.append(jacobian_time)		
print zip(sizes, times_ilu, residual_times_ilu, jacobian_times_ilu)

# linear solver GMRES/GAMG
times_gamg = []
residual_times_gamg = []
jacobian_times_gamg = []
stage = 'Main Stage'
solver_method = 'KSPSolve'
index = 0
event = 'time'
for k in range(5):
	Nx = 10 * 2 ** k
	modname = 'perf_gamg%d' % k
	options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-log_view', ':%s.py:ascii_info_detail' % modname, 
		'-ksp_type gmres -pc_type gamg']
	os.system('./ex5 ' + ' '.join(options))
	perfmod = __import__(modname)
	size = Nx**2
	time = perfmod.Stages[stage]['KSPSolve'][index]['time']
	residual_time = perfmod.Stages[stage]['SNESFunctionEval'][index]['time']
	jacobian_time = perfmod.Stages[stage]['SNESJacobianEval'][index]['time']
	times_gamg.append(time)
	residual_times_gamg.append(residual_time)
	jacobian_times_gamg.append(jacobian_time)		
print zip(sizes, times_gamg, residual_times_gamg, jacobian_times_gamg)

from pylab import legend, plot, loglog, show, title, xlabel, ylabel, figure, subplot
# 1.5
figure(1)
subplot(211)
plot(sizes, times)
plot(sizes, times_ilu)
plot(sizes, times_gamg)
legend(['SNESSolve', 'KSPSolve GMRES/ILU', 'KSPSolve GMRES/GAMG'], loc='upper left')
title('SNES, GMERES/GAMG, GMERES/ILU ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')

subplot(212)
loglog(sizes, times)
loglog(sizes, times_ilu)
loglog(sizes, times_gamg)
legend(['SNESSolve', 'KSPSolve GMRES/ILU', 'KSPSolve GMRES/GAMG'], loc='upper left')
title('SNES, GMERES/GAMG, GMERES/ILU ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

# 1.6 SNESSolve
figure(2)
subplot(211)
plot(sizes, times)
plot(sizes, residual_times)
plot(sizes, jacobian_times)
legend(['SNESSolve', 'SNESSolve Residual', 'SNESSolve Jacobian'], 
	loc='upper left')
title('SNES Solve time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')

subplot(212)
loglog(sizes, times)
loglog(sizes, residual_times)
loglog(sizes, jacobian_times)
legend(['SNESSolve', 'SNESSolve Residual', 'SNESSolve Jacobian'], 
	loc='upper left')
title('SNES Solve time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

# 1.6 ILU
figure(3)
subplot(211)
plot(sizes, times_ilu)
plot(sizes, residual_times_ilu)
plot(sizes, jacobian_times_ilu)
legend(['KSPSolve GMRES/ILU', 'KSPSolve GMRES/ILU Residual', 'KSPSolve GMRES/ILU Jacobian'], 
	loc='upper left')
title('KSPSolve GMRES/ILU time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')

subplot(212)
loglog(sizes, times_ilu)
loglog(sizes, residual_times_ilu)
loglog(sizes, jacobian_times_ilu)
legend(['KSPSolve GMRES/ILU', 'KSPSolve GMRES/ILU Residual', 'KSPSolve GMRES/ILU Jacobian'], 
	loc='upper left')
title('KSPSolve GMRES/ILU time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()

# 1.6 GAMG
figure(4)
subplot(211)
plot(sizes, times_gamg)
plot(sizes, residual_times_gamg)
plot(sizes, jacobian_times_gamg)
legend(['KSPSolve GMRES/GAMG', 'KSPSolve GMRES/GAMG Residual', 'KSPSolve GMRES/GAMG Jacobian'], 
	loc='upper left')
title('KSPSolve GMRES/GAMG time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')

subplot(212)
loglog(sizes, times_gamg)
loglog(sizes, residual_times_gamg)
loglog(sizes, jacobian_times_gamg)
legend(['KSPSolve GMRES/GAMG', 'KSPSolve GMRES/GAMG Residual', 'KSPSolve GMRES/GAMG Jacobian'], 
	loc='upper left')
title('KSPSolve GMRES/GAMG time, Residual, Jacobian ex5')
xlabel('Problem Size $N$')
ylabel('Time (s)')
show()
